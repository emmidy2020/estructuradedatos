
#include <iostream>
#include <string>
#include "book.h"
#include "link.h"
#include "llist.h"

using namespace std;

    template <typename E>
    void lprint(List<E>& L)
    {

        int currpos = L.currPos();

        L.moveToStart();

        cout << "< ";

        while (L.currPos()<L.length())
        {
            cout << *L.getValue() << " ";
            L.next();
        } 
        cout << ">\n";

        L.moveToPos(currpos); // se devuelve a la posicion inicial
    }

    template <typename E>
    bool depurador(List<E>& L)
    {

        bool OUT = false; // variable de salida
        int currpos = L.currPos();


        L.moveToStart(); // Mueve al inicio de la lista

        int contList = 0;
        while (L.currPos()<L.length())
        {
            string varActual = *L.getValue();

            //for(int i=1; i<L.length() ; i++)
            int i = contList + 1;
            while (i<L.length())
            {
                L.moveToPos(i);
                string auxVariable = *L.getValue();

                
                if(varActual.compare(auxVariable) == 0)
                {
                    cout <<" Elemento repetido " << auxVariable << "\n";
                    L.remove();
                    OUT = true;
                }
                i++;
            }
            contList++;
            L.moveToPos(contList);
        }

        L.moveToPos(currpos); // se devuelve a la posicion inicial

        return OUT;
    }

// Declaracion del main de Listas
int main()
{
    LList<string*> L1;
    L1.moveToStart();

    L1.insert(new string("Luis"));
    L1.insert(new string("Juan"));
    L1.insert(new string("Maria"));
    L1.insert(new string("Pedro"));
    L1.insert(new string("Luis"));
    L1.insert(new string("Jose"));
    L1.insert(new string("Luis"));
    L1.insert(new string("Maria"));
    L1.insert(new string("Jesus"));
    L1.insert(new string("Rosa"));
    L1.insert(new string("Luis"));
    L1.insert(new string("Pablo"));

    cout << "\n\n Impresion de la lista inicial: \n";
    lprint(L1);

    cout << "\n Start Depuracion \n";
    depurador(L1);
    cout << " End Depuracion \n";

    cout << "\n Impresion de la lista depurada: \n";
    lprint(L1);

    return EXIT_SUCCESS;
}
